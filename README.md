# Helm install image

Minimal images with [Helm](https://helm.sh/) and [kubectl](https://kubernetes.io/docs/reference/kubectl/) installed. Built for GitLab's cluster integration.

There are three variants:


## Helm v3

Contains Helm 3 and kubectl

### Binaries

- Helm v3:
  - `/usr/bin/helm`
- kubectl:
  - `/usr/bin/kubectl`

### Image

```plaintext
registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image:3.7.2-kube-1.21.5-alpine-3.15
```

## Helm 2to3

Contains both Helm v2 and Helm v3, as well as the `helm-2to3` plugin installed on Helm v3 to facilitate Helm v3 migrations.

### Binaries

- Helm v2:
  - `/usr/bin/helm2`
  - `/usr/bin/tiller`
- Helm v3:
  - `/usr/bin/helm3`
- kubectl:
  - `/usr/bin/kubectl`

### Image

```plaintext
registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/helm-2to3:2.17.0-3.7.2-kube-1.21.5-alpine-3.15
```
